package com.clave.s01activity;

public class S01Activity {

    public static void main(String[] args){

        int stock = 10;
        double price = 99.99;
        char size = 'L';
        boolean available = true;

        System.out.println("Size: " + size + "\nStock: " + stock + "\nAvailable: " + available + "\nPrice: " + price);
    }

}
